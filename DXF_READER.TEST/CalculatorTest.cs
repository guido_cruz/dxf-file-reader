﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DXF_READER.model;
using System.Collections.Generic;

namespace DXF_READER.TEST
{
    [TestClass]
    public class CalculatorTest
    {
        Calculator calculator;

        [TestInitialize]
        public void Setup()
        {
            calculator = new Calculator();
        }

        [TestMethod]
        public void LineDimentionsTest()
        {
            //line dimention
            var LineResult = calculator.PointsDimention(new Position(14.5591, 20.8413),
                                                        new Position(51.1732, 20.8413));
            //the line dimention expected is 36.6141
            Assert.AreEqual(36.6141, LineResult);

            var LineResult1 = calculator.PointsDimention(new Position(48.9515, 13.1192),
                                                        new Position(69.5692, -1.8813));
            //the line dimention expected is exactly 25.497147949133442
            Assert.AreEqual(25.497147949133442, LineResult1);
        }

        [TestMethod]
        public void CircleDimentionsTest()
        {
            //circle perimeter
            var CircleResult = calculator.CirclePerimeter(5.954874697730929);
            Assert.AreEqual(37.415581206878457, CircleResult);

            //circle perimeter
            var CircleResult1 = calculator.CirclePerimeter(8.71011548308337);
            Assert.AreEqual(54.72726962714686, CircleResult1);

            //circle perimeter
            var CircleResult2 = calculator.CirclePerimeter(13.86508177234651);
            Assert.AreEqual(87.116878074851087, CircleResult2);
        }

        [TestMethod]
        public void ArcDimentionsTest()
        {
            //arc perimeter
            var ArcResult = calculator.ArcPerimeter(23.2574, 53, 167);
            Assert.AreEqual(46.274675421679525, ArcResult);

            //arc perimeter
            var ArcResult1 = calculator.ArcPerimeter(4.826128977284072, 182.5397882462827, 134.7767383942254);
            Assert.AreEqual(26.300293070342459, ArcResult1);
        }

        [TestMethod]
        public void PolylineDimentionTest()
        {
            //polyline with 5 points perimeter
            //List<Position> positions = new List<Position>();
            //positions.Add(new Position(16.846477, 16.634));
            //positions.Add(new Position(47.708, 16.634));
            //positions.Add(new Position(47.70818, 3.57652));
            //positions.Add(new Position(16.57966, 3.57652));
            //positions.Add(new Position(16.846477, 16.634));

            ////var polylineResult =  calculator.PolylineDimention(5, positions);
            ////Assert.AreEqual(88.107728790656424, polylineResult);

            ////polyline with 9 points perimeter
            //List<Position> positions2 = new List<Position>();
            //positions2.Add(new Position(56.06841095356307, 18.67753941407775));
            //positions2.Add(new Position(77.23580359966084, 19.47700528564047));
            //positions2.Add(new Position(75.45703100307415, 5.441937355691905));
            //positions2.Add(new Position(61.13791250570077, 12.72596000001482));
            //positions2.Add(new Position(61.13791250570077, 2.332903336733323));
            //positions2.Add(new Position(69.49814329189687, -0.5096420583710284));
            //positions2.Add(new Position(61.31578990461313, -5.484096555298066));
            //positions2.Add(new Position(52.68874315930216, 2.244073758452302));
            //positions2.Add(new Position(56.06841095356307, 18.67753941407775));

            //var polylineResult2 = calculator.PolylineDimention(9, positions2);
            //Assert.AreEqual(108.55398959192075, polylineResult2);

            //FileReader reader = new FileReader();
            //reader.read();
        }
    }
}
