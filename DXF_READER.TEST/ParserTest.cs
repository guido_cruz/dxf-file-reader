﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DXF_READER.model.Parser;

namespace DXF_READER.TEST
{
    [TestClass]
    public class ParserTest
    {

        Parser parser;

        [TestInitialize]
        public void Setup()
        {
            parser = new Parser();
        }

        [TestMethod]
        public void TotalDimentionTest()
        {
            parser.setPath("filesToRead/circulo.dxf");
            parser.Parse();
            double result = parser.getTotalDimention();
            Console.Write(result);
        }
    }
}
