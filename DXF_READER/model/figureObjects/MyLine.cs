﻿using netDxf.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DXF_READER.model
{
    public class MyLine : Line, Figure
    {

        public MyLine()
        {
        }

        public double getDimention()
        {
            Position init = new Position(StartPoint.X, StartPoint.Y);
            Position end = new Position(EndPoint.X, EndPoint.Y);

            return Calculator.getInstance().PointsDimention(init, end);
        }
    }
}
