﻿using netDxf.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DXF_READER.model.figureObjects
{
    public class MyCircle : Circle, Figure
    {
        public MyCircle(double radius)
        {
            Radius = radius;
        }

        public MyCircle()
        {
        }

        public double getDimention()
        {
            return Calculator.getInstance().CirclePerimeter(Radius);
        }
    }
}
