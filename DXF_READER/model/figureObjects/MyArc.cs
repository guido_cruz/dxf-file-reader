﻿using netDxf.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DXF_READER.model.figureObjects
{
    public class MyArc : Arc, Figure
    {
        public MyArc()
        {
        }

        public MyArc(double circleRadius, double initialAngle, double endAngle)
        {
            Radius = circleRadius;
            StartAngle = initialAngle;
            EndAngle = endAngle;
        }

        public double getDimention()
        {
            return Calculator.getInstance().ArcPerimeter(Radius, StartAngle, EndAngle);
        }
    }
}
