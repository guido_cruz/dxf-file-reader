﻿using netDxf.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DXF_READER.model.figureObjects
{
    public class MyLwPolyline : LwPolyline, Figure
    {
        public MyLwPolyline()
        {
            totalDimention = 0;
        }

        public double getDimention()
        {
            for (int i = 0; i < Vertexes.Count - 1; i++)
            {
                totalDimention = totalDimention + Calculator.getInstance().LwPolilyneDimention(Vertexes[i], Vertexes[i + 1]);
            }
            return totalDimention;
        }

        private double totalDimention;
    }
}
