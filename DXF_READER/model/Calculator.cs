﻿using netDxf.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DXF_READER.model
{
    public class Calculator
    {
        private static Calculator instance;

        public static Calculator getInstance()
        {
            if (instance == null)
            {
                instance = new Calculator();
            }
            return instance;
        }

        public double PointsDimention(Position position1, Position position2)
        {
            double aux = Math.Pow((position2.Xaxis - position1.Xaxis), 2) +
                         Math.Pow((position2.Yaxis - position1.Yaxis), 2);

            return Math.Sqrt(aux);
        }

        public double ArcPerimeter(double radius, double initialAngle, double endAngle)
        {
            double internAngle = GetInternAngle(initialAngle, endAngle);
            double circlePerimeter = CirclePerimeter(radius);

            return (circlePerimeter * internAngle) / 360;
        }

        public double ArcPerimeter(double radius, double internAngle)
        {
            double circlePerimeter = CirclePerimeter(radius);

            return (circlePerimeter * internAngle) / 360;
        }

        //public double PolylineDimention(int pointsNumber, List<Position> positions)
        //{
        //    double acumulated = 0;
        //    for (int i = 0; i < pointsNumber - 1; i++)
        //    {
        //        Position initialPosition = positions[i];
        //        Position endPosition = positions[i + 1];

        //        acumulated = acumulated + PointsDimention(initialPosition, endPosition);
        //    }
        //    return acumulated;
        //}

        public double LwPolilyneDimention(LwPolylineVertex init, LwPolylineVertex end)
        {
            double aux = Math.Pow((end.Position.X - init.Position.X), 2) +
                                    Math.Pow((end.Position.Y - init.Position.Y), 2);
            double result = Math.Sqrt(aux);
            if (init.Bulge != 0)
            {
                double buldAux = 4 * Math.Atan(init.Bulge);
                double internAngle = (4 * Math.Atan(init.Bulge) * 180) / Math.PI;

                double radius = (result / 2)/Math.Sin(buldAux/2);
                result = ArcPerimeter(radius, internAngle);
            }
            return result;
        }

        public double CirclePerimeter(double radius)
        {
            return (2 * Math.PI * radius);
        }

        private double GetInternAngle(double initialAngle, double endAngle)
        {
            double internAngle = 0;
            if (endAngle > initialAngle)
            {
                internAngle = endAngle - initialAngle;
            }
            else
            {
                double aux = initialAngle - endAngle;
                internAngle = 360 - aux;
            }
            return internAngle;
        }
    }
}
