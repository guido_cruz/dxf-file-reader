﻿//using DXF_READER.model.figureObjects;
using DXF_READER.model.figureObjects;
using netDxf;
using netDxf.Entities;
using System.Collections.Generic;

namespace DXF_READER.model.Parser
{
    public class Parser
    {
        public List<Figure> AllFigures { get; set; }

        public Parser()
        {
            AllFigures = new List<Figure>();
        }

        public Parser(string path)
        {
            AllFigures = new List<Figure>();
            setPath(path);
            fileName = string.Empty;
        }

        public double getTotalDimention()
        {
            double totalDimention = 0;
            foreach (Figure currentFigure in AllFigures)
            {
                totalDimention = totalDimention + currentFigure.getDimention();
            }
            return totalDimention;
        }

               
        public string getFileName()
        {
            return fileName;
        }

        public void setPath(string path)
        {
            string aux = path.Replace("\\", "/");
            dxfLoad = DxfDocument.Load(aux);
        }

        public void Parse()
        {
            if (dxfLoad != null)
            {
                fileName = "File Name: " + dxfLoad.Name;
                fillArcs();
                fillCircles();
                fillPolylines();
                fillLine();
            }
            else
            {
                fileName = "Supported Autocad versions: 2000, 2004, 2007, 2010, 2013, 2018";
            }
        }

        private void fillPolylines()
        {
            foreach (LwPolyline lwPolyline in dxfLoad.LwPolylines)
            {
                MyLwPolyline currentPolyline = new MyLwPolyline();
                //currentPolyline.Vertexes.Add(dxfLoad.LwPolylines[i].Vertexes[0]);
                addPolilines(lwPolyline, currentPolyline);
                if (lwPolyline.IsClosed)
                {
                    currentPolyline.Vertexes.Add(currentPolyline.Vertexes[0]);
                }
                AllFigures.Add(currentPolyline);
            }
        }

        private void addPolilines(LwPolyline poliline, LwPolyline toAdd)
        {
            for (int i = 0; i < poliline.Vertexes.Count; i++)
            {
                toAdd.Vertexes.Add(poliline.Vertexes[0]);
            }
        }

        private void fillLine()
        {
            foreach (Line line in dxfLoad.Lines)
            {
                MyLine currentLine = new MyLine();
                currentLine.StartPoint = line.StartPoint;
                currentLine.EndPoint = line.EndPoint;

                AllFigures.Add(currentLine);
            }
        }

        private void fillCircles()
        {
            foreach (Circle circle in dxfLoad.Circles)
            {
                MyCircle currentCircle = new MyCircle();
                currentCircle.Radius = circle.Radius;
                AllFigures.Add(currentCircle);
            }
        }

        private void fillArcs()
        {
            foreach (Arc arc in dxfLoad.Arcs)
            {
                MyArc currentArc = new MyArc();
                currentArc.StartAngle = arc.StartAngle;
                currentArc.EndAngle = arc.EndAngle;
                currentArc.Radius = arc.Radius;

                AllFigures.Add(currentArc);
            }
        }

        private DxfDocument dxfLoad;
        private string fileName;
    }
}
