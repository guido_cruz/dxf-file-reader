﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DXF_READER.model
{
    public class Position
    {
        public double Xaxis { get; set; }
        public double Yaxis { get; set; }

        public Position()
        {

        }
        public Position(double Xaxis, double Yaxis)
        {
            this.Xaxis = Xaxis;
            this.Yaxis = Yaxis;
        }
    }
}
