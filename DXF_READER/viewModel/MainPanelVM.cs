﻿using DXF_READER.model.Parser;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace DXF_READER.viewModel
{
    public class MainPanelVM : INotifyPropertyChanged
    {

        public string Result
        {
            get { return result; }

            set
            {
                result = value;
                OnPropertyChanged("Result");
            }
        }

        public string FileName
        {
            get { return fileName; }
            set
            {
                fileName = value;
                OnPropertyChanged("FileName");
            }
        }

        public string FilePath
        {
            get { return filePath; }

            set
            {
                filePath = value;
                OnPropertyChanged("FilePath");
            }
        }

        public ICommand ChooseFile
        {
            get { return chooseFile; }
            set { chooseFile = value; }
        }

        public MainPanelVM()
        {
            result = "Result";
            chooseFile = new Command(ChooseFileMethod);
            filePath = "File Path";
        }

        public void ChooseFileMethod(object obj)
        {
            string path;
            OpenFileDialog file = new OpenFileDialog();
            file.Filter = "dxf files (*.dxf)|*.dxf";
            file.FilterIndex = 1;
            file.RestoreDirectory = true;

            bool? userClickedOk = file.ShowDialog();

            if (userClickedOk == true)
            {
                path = file.FileName;
                FilePath = path;
                parser = new Parser(path);
                parser.Parse();
                FileName = parser.getFileName();
                Result = parser.getTotalDimention().ToString();
            }
        }

        private void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        private string result;
        private string filePath;
        private string fileName;
        private Parser parser;
        private ICommand chooseFile;

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
